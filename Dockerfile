FROM rocker/r-ver:latest

RUN apt-get update && apt-get install -y \
git-core \
libcurl4-openssl-dev \
libgit2-dev \
libicu-dev \
libssl-dev \
libxml2-dev \
pandoc \
curl \
gdebi-core \
libfontconfig1-dev \
libxt6 \
fontconfig \
&& rm -rf /var/lib/apt/lists/*

RUN curl -L https://github.com/quarto-dev/quarto-cli/releases/download/v1.7.4/quarto-1.7.4-linux-amd64.deb -o quarto-linux-amd64.deb
RUN gdebi --non-interactive quarto-linux-amd64.deb

#RUN R -e "install.packages(c('tidyverse','quarto','knitr','rmarkdown','gt','here','googlesheets4', 'gert','purrr','gtExtras','svglite', 'devtools'),repos='http://cran.rstudio.com')"
RUN R -e "install.packages( 'pak', repos='http://cran.rstudio.com')"
RUN R -e "pak::pkg_install(c('tidyverse','quarto','knitr','rmarkdown','gt', 'gert','purrr','gtExtras','svglite'))"
